function configure($stateProvider) {
    $stateProvider.state("childTwoComponent", {
        parent: "parent",
        component: "childTwoComponent",
        data: {
            isChild: true,
            navTitle: "Child Two Component",
            moduleTitle: "Child Two Component",
            isMenuItem: true
        }
    });
}
configure.$inject = ['$stateProvider'];
export default configure;
//# sourceMappingURL=child-two.config.js.map